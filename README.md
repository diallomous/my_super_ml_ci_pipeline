# My Super ML Pipeline - GitLab CI/CD

This project is configured to automatically execute a series of tasks using GitLab CI/CD (Continuous Integration and Continuous Deployment) whenever changes are pushed to the repository. The workflow includes testing CSV columns, data cleaning, model training, and selecting the best model based on mean absolute error (MAE).

## Workflow Overview

After every push to the repository, the following steps are executed as part of the GitLab CI/CD workflow:

1. **Testing CSV Columns** (`unit_test.py`):
   - The `unit_test.py` script is executed to test whether a list of required columns is available in a CSV file.
   - The script checks if the required columns are present and raises an error if any column is missing.

2. **Data Cleaning** (`process.py`):
   - The `process.py` script is run to perform data processing tasks such as handling missing values and performing data transformations on the input data.
   - The cleaned data is saved as `train.csv` and `test.csv` files.

3. **Model Training and Selection** (`train.py`):
   - The `train.py` script trains three different models on the cleaned data.
   - For each model, hyperparameter tuning is performed, and the model with the lowest mean absolute error (MAE) is selected as the best model.
   - The results of the model comparison, including the MAE scores, are saved in a `results.txt` file.

4. **Output**:
   - After the workflow completes, the following outputs are available:
     - `train.csv` and `test.csv`: Cleaned data files.
     - `results.txt`: Results of the model comparison and MAE scores.
     - The best model trained using the data.

## Running the Workflow Manually

You can also manually trigger the GitLab CI/CD workflow to run:

1. Open your GitLab repository.
2. Click on the "CI/CD" tab.
3. Select the pipeline named "Main Pipeline" or a similar name.
4. Click the "Run Pipeline" button to manually trigger the workflow.

## Configuring Workflow Behavior

The workflow behavior can be customized by modifying the `.gitlab-ci.yml` file in your project repository. You can adjust the triggers, jobs, and environment variables as needed for your project.

## Dependencies

Make sure you have the required dependencies installed for running the scripts. You might need to set up a virtual environment and install the necessary packages using `pip` or a similar tool.

## Notes

- Make sure to replace placeholders like `unit_test.py`, `process.py`, and `train.py` with the actual filenames of your scripts.
- Customize the script behavior, column names, and data processing steps according to your project requirements.
- This README provides a high-level overview of the workflow. Refer to the script files and GitLab CI/CD configuration for more details.

For any questions or issues related to the workflow or project, feel free to open an issue or contact the project maintainers at `diallomous@gmail.com`.